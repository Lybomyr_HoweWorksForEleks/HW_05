﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


/// <summary>
/// here write test!!!!!!!
/// </summary>
namespace HomeWork5
{
    static class MyValidator
    {
        public static bool ValidationIsLetter(string word)
        {
            bool result = true;
            foreach (char item in word)
            {
                if (!Char.IsLetter(item))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        public static bool ValidationFromBaseLib_Range(int elemnent, int length)
        {
            bool result = true;

            var results = new List<ValidationResult>();
            var context = new ValidationContext(elemnent);
            var validateAtrributesForAge = new List<ValidationAttribute>();

            validateAtrributesForAge.Add(new RequiredAttribute());
            validateAtrributesForAge.Add(new RangeAttribute(1, length));

            if (!Validator.TryValidateValue(elemnent, context, results, validateAtrributesForAge))
            {
                result = false;
            }
            return result;
        }

        public static bool ValidationromBaseLib_Length(string elemnent, int length)
        {
            bool result = true;
            var results = new List<ValidationResult>();
            var context = new ValidationContext(elemnent);
            var validateAtrributesForLength = new List<ValidationAttribute>();

            validateAtrributesForLength.Add(new RequiredAttribute());
            validateAtrributesForLength.Add(new StringLengthAttribute(length));

            if (!Validator.TryValidateValue(elemnent, context, results, validateAtrributesForLength))
            {
                result = false;
            }
            return result;
        }
    }
}
