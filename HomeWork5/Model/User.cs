﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace HomeWork5
{
    class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Work { get; set; }

        public User(string fname, string lname, int age, string work)
        {
            this.FirstName = fname;
            this.LastName = lname;
            this.Age = age;
            this.Work = work;
        }

        public override string ToString()
        {
            return "|Name - " + $"{FirstName}" + " |Last name - " 
                + $"{LastName}" + " |Age - " + $"{Age}" + " | Work place - " + $"{Work}";
        }
    }
}
