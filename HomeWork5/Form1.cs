﻿using System;
using System.Windows.Forms;

namespace HomeWork5
{
    public partial class Form1 : Form
    {
        CreationUsersPresenter newUser;
        public Form1()
        {
            InitializeComponent();
            newUser = new CreationUsersPresenter(this, textBox1, textBox2, textBox3, textBox4);

        }

        public event EventHandler saveFile = null;
        public event EventHandler creationUser = null;

        private void button3_Click(object sender, EventArgs e)
        {
            creationUser.Invoke(sender, e);
            
               listBox1.Items.Add(newUser.result);
            
            
            
        }
        private void button5_Click(object sender, EventArgs e)
        {
            File file = new File(this, this.saveFileDialog1);
            saveFile.Invoke(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(SearchUser.Search(textBox6.Text, textBox7.Text).ToString());
        }
    }
}
